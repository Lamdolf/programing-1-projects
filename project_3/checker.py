import json


def valid_task(name, day, month, relevance):

    aux = True
    invalid_31 = {'Abril', 'Junio', 'Septiembre', 'Noviembre'}
    valid_days = []
    for i in range(31):
        valid_days.append(str(i+1))

    if name == '' or day == '':
        aux = False

    elif day not in valid_days:
        aux = False

    elif int(day)>29 and month == 'Febrero':
        aux = False

    elif month == None or relevance == None:
        aux = False

    elif int(day) == 31 and month in invalid_31:
        aux = False

    return aux


def valid_class(name):

    aux = True

    with open('classes.json') as file:
        data = json.load(file)
    file.close()

    if name in data:
        aux = False

    elif name == '':
        aux = False

    return aux


def valid_block(day, block):

    aux = True

    with open('classes.json') as file:
        data = json.load(file)
    file.close()

    for names in data:
        for days in data[names]:
            for hour in days:

                if hour == day and days[hour] == block:
                    aux = False

    return aux


def valid_partner(name, lastname, career, phone, mail):

    aux = True
    with open('partners.json') as file:
        data = json.load(file)
    file.close()

    try:
        i = int(phone)

    except ValueError:
        aux = False

    if name == '' or lastname == '':
        aux = False

    elif career == '' or mail == '':
        aux = False

    elif name == '' or len(phone) != 8:
        aux = False

    for partners in data:
        if partners['mail'] == mail:
            aux = False

        elif partners['phone'] == phone:
            aux = False

    return aux
